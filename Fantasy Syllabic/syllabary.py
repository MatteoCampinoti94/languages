import string
import sys
import re


symbols = 'o*-='
# symbols = '/~-\\'
# symbols = '/:|\\'

base = len(symbols)
bytel = 5


def dec2qua(n):
    ret = []
    while n >= base:
        n, v = n // base, n % base
        ret = [v] + ret
    ret = [n] + ret

    ret = [0]*(bytel-len(ret)) + ret

    return ret


def qua2dec(q):
    dec = 0
    for i in range(0, bytel):
        dec += q[i]*(base**(bytel-1-i))

    return dec


def generatesymbols():
    global base
    global combinations
    global symbols_index

    base = len(symbols)

    combinations = [
        ''.join([symbols[n] for n in dec2qua(i)])
        for i in range(0, base**bytel)
        ]

    symbols_index = {
        v: i
        for i, v in enumerate(symbols, 0)
    }

symbols_index = {
        v: i
        for i, v in enumerate(symbols, 0)
    }

letters = list(string.ascii_lowercase)
digits = list(string.digits)
punctuation = list(string.punctuation)
syllables = [i+j for i in letters for j in letters]
combinations = [
    ''.join([symbols[n] for n in dec2qua(i)])
    for i in range(0, base**bytel)
    ]

re_ascii = re.compile(r'[^\x20-\x7E\n]')
re_space = re.compile(r'[ ]{1,}')
re_split = re.compile(r'([A-Za-z]{2}|.)')
re_script = re.compile(r'([o\*\-=]{5}|[ ]{5}|\|)( |$)')
re_box = re.compile(r'^[\-+]+$')


def string2symbol(s):
    s = s.lower()
    s = re_space.sub(' ', s)
    s = re_ascii.sub('', s)
    s = [i for i in re_split.split(s) if i]
    ret = []

    for syllable in s:
        if syllable[0] not in digits+letters+punctuation:
            ret.append(syllable)
            continue
        elif syllable in digits:
            n = int(syllable)
        elif syllable in letters:
            n = ord(syllable) - 97
            n += 10
        elif syllable in punctuation:
            n = punctuation.index(syllable)
            n += 10 + 26 + 676
        else:
            n1 = ord(syllable[0]) - 97
            n2 = ord(syllable[1]) - 97

            n = (26*n1 + n2)
            n += 10 + 26

        ret.append(combinations[n])

    return ret


def symbol2string(symbol):
    if symbol == ' '*5:
        return ' '

    qubyte = [symbols_index[bit] for bit in symbol]
    value = qua2dec(qubyte)

    if value < 10:
        return str(value)
    elif value < 36:
        return chr(value-10+97)
    elif value < 712:
        value -= 10 + 26
        l1 = value // 26
        l2 = value % 26
        return chr(l1+97)+chr(l2+97)
    else:
        value -= 712
        return punctuation[value]


def write_fmt(s, len_group=5, len_line=100, len_block=15, sep=' ', end='|'):
    # vertical: len_group=1, len_line=20, len_block=20, sep=' ', end='|'
    # horizontal: len_group=5, len_line=100, len_block=15, sep=' ', end='|'
    tmp = ''.join(s).split('\n')
    out = [[]]
    ret = ''

    for line in tmp:
        line = line.replace(' ', ' '*len_group)
        while len(line):
            out.append(line[:len_line])
            line = line[len_line:]
        out.append([])

    while out:
        for i in range(0, len_line, len_group):
            for line in out[:len_block]:
                if not line:
                    ret += end
                elif len(line) > i:
                    ret += f'{line[i:i+len_group]}'
                else:
                    ret += ' '*len_group
                ret += sep
            ret += '\n'
        ret += '\n'
        out = out[len_block:]

    ret = ret.strip('\n')
    print(ret)


def read_fmt(text):
    text_tmp = text.split('\n')
    text_tmp = [line for line in text_tmp if not re_box.match(line)]
    text_tmp = [re_script.split(line) for line in text_tmp]
    text_tmp = [[x for x in line if re_script.search(x)] for line in text_tmp]
    text_tmp = [line for line in text_tmp if line]
    text_tmp = [line + ['     ']*(15-len(line)) for line in text_tmp]

    text = []
    while text_tmp:
        section = text_tmp[:20]
        text_tmp = text_tmp[20:]
        line = []

        for j in range(0, 15):
            for i in range(0, len(section)):
                line.append(section[i][j])

            text.append(line)
            line = []

    string = ''

    for line in text:
        if line[0] == '|':
            string += '\n'
            continue

        for symbol in line:
            string += symbol2string(symbol)

    string = re.sub(r'[ ]+', ' ', string.strip())
    return string


def string2script(s):
    s = string2symbol(s)
    print(''.join(s))


def string2script_fmt(s):
    s = string2symbol(s)
    write_fmt(s)


def script2string_fmt(s):
    print(read_fmt(s))

if __name__ == "__main__":
    if not sys.argv[1:]:
        for n, syllable in enumerate(digits+letters+syllables+punctuation, 0):
            print(f'{n:04}', f'{syllable:<2s}', combinations[n])
    else:
        if sys.argv[1] == '--2script' and sys.argv[2:]:
            s = ' '.join(sys.argv[2:])
            string2script_fmt(s)
        elif sys.argv[1] == '--2string' and sys.argv[2:]:
            s = ' '.join(sys.argv[2:])
            script2string_fmt(s)
        elif sys.argv[1] == '--file2script' and sys.argv[2:]:
            with open(sys.argv[2], 'r') as f:
                s = ''.join(f.readlines())
            string2script_fmt(s)
        elif sys.argv[1] == '--file2string' and sys.argv[2:]:
            with open(sys.argv[2], 'r') as f:
                s = ''.join(f.readlines())
            script2string_fmt(s)
        else:
            s = ' '.join(sys.argv[1:])
            string2script_fmt(s)
