import os
import string

def conv_int(n):
    n = int (n)
    sign =  +1 if n >= 0 else 0
    n    *= -1 if n < 0 else 1
    n    =  bin(n)[2:]
    n    =  n.zfill(8*((len(n)//8)+1))
    n    =  n[::-1]


    N = []
    while len(n):
        ni =  n[0:8]
        n  =  n[8:]
        ni =  ('◑' if sign else '◐') + ni.replace('1','●').replace('0','○')

        N += [ni]

    while len(N) > 1 and '●' not in N[-1]:
        del N[-1]

    return N

def conv_sym(s):
    symbA = '+-*^%~/()\\[]|{}<>=.,;:!?"\'_'
    symbB = '#&`@þÞàáäảåâāãǎȧçÇæÆœŒøØßðÐ'

    if s not in symbA+symbB:
        return ''

    symb  = ['○','○','○','○','○','○','○','○','○']

    if s in symbA:
        group = '⚈'

        A = int(symbA.index(s)/9)
        B = int((symbA.index(s) - A*9)/3)
        C = int(symbA.index(s) - A*9 - B*3)
    else:
        group = '⚉'

        A = int(symbB.index(s)/9)
        B = int((symbB.index(s) - A*9)/3)
        C = int(symbB.index(s) - A*9 - B*3)

    symb[A] = group
    symb[3+B] = '●'
    symb[6+C] = '●'

    return ''.join(symb)

def conv_chr(c):
    upper = c.isupper()
    c = c.lower()

    if c == ' ':
        return '○○○○○○○○○'

    elif c in ('\n', '\r'):
        return '●●●●●●●●●'

    elif c in string.ascii_lowercase:
        c = ord(c) - 97
        A = int(c//9)
        B = int((c - A*9)//3)
        C = c - A*9 - B*3
        ts = ['○','○','○','○','○','○','○','○','○']
        ts[A]   = '◓' if upper else '◒'
        ts[3+B] = '●'
        ts[6+C] = '●'

        return ''.join(ts)


def linear_ts(s, sep='', n=0, n_sep='', list=False):
    i  = 0
    st = []
    while i < len(s):
        if s[i] in string.ascii_letters + '\n\r ':
            st += [conv_chr(s[i])]

        elif s[i] in string.digits:
            d = ''
            while i < len(s) and s[i] in string.digits:
                d += s[i]
                i += 1

            i -= 1
            st += conv_int(d)

        elif s[i] == '-' and i < len(s)-1 and s[i+1] in string.digits:
            d =  s[i]
            i += 1
            while i < len(s) and s[i] in string.digits:
                d += s[i]
                i += 1

            i -= 1
            st += conv_int(d)

        else:
            st += [conv_sym(s[i])]

        i+= 1

    if list:
        return st

    s = st

    if n > 0:
        s = []
        while len(st):
            s  += [n_sep.join(st[0:n])]
            st =  st[n:]

    return sep.join(s)

def linr2mtrx(c):
    c = [c[0]+c[3]+c[6], c[1]+c[4]+c[7], c[2]+c[5]+c[8]]

    return c

def matrix_ts(s, sep=' ', n=0, sep_row='\n', list=False):
    s = [linr2mtrx(c) for c in linear_ts(s, list=True)]

    if list:
        return s

    rows = [[], [], []]
    for c in s:
        rows[0] += [c[0]]
        rows[1] += [c[1]]
        rows[2] += [c[2]]

    s = ''

    if n <= 0:
        n = os.get_terminal_size()[0]
        n = int(n // ((3+len(sep))))

    while len(rows[0]):
        for i in range(0, 3):
            row = rows[i][:n]
            rows[i] = rows[i][n:]
            s += sep.join(row)
            s += '\n' if len(rows[-1]) else ''

        s += sep_row if len(rows[0]) else ''

    return s
