# TinySpeak

TinySpeak can either be written by hand or with the use of a digital keyboard.

◒ ¦ ◓ | ◑ ¦ ◐ | ⚈ ¦ ⚉ | ✛
:---: | :---: | :---: | :-:
 ↖ ○  |  ○ ↑  |  ○ ↗  | ⇤
 ← ○  |  ○ ·  |  ○ →  | ↳
 ↙ ○  |  ○ ↓  |  ○ ↘  | ↻

letters | numbers | symbols | cursor
:-----: | :-----: | :-----: | :----:
 C1 G1  |  G4 C4  |  G7 C7  |  back
 C2 G2  |  G5 C5  |  G8 C8  | enter
 C3 G3  |  G6 C6  |  G9 C9  | reset

`letter`, `numbers` and `symbols` double keys select lower/uppercase letters, positive/negative numbers, symbols A/B.

`cursor` key switches cursor mode on/off.

`Gx` keys turn the single dots of the symbol matrix to 1/0.

`Cx` keys are used in cursor mode. `C5` opens the current selection into edit mode and exits cursor mode.

`back` key returns to the previous entered symbol in edit mode deleting the current one.

`enter` confirms the current symbol and goes to the next one. When inserting letters using `G7-9` automatically confirms the symbol.

`reset` turns `G1-9` to 0.
