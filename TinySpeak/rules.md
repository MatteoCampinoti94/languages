# TinySpeak

Simplified script to express any ASCII letter or symbol and any number.

Any letter, symbol or number can be expressed using a 3x3 grid of dots or a string of 9 dots.<br>
The dots are to be read column by column, from top to bottom then left to right.

`0`&nbsp;`3`&nbsp;`6`<br>
`1`&nbsp;`4`&nbsp;`7`<br>
`2`&nbsp;`5`&nbsp;`8`&nbsp;

`0`&nbsp;`1`&nbsp;`2`&nbsp;`3`&nbsp;`4`&nbsp;`5`&nbsp;`6`&nbsp;`7`&nbsp;`8`

The first dot in the first column identifies the symbol:<br>
◒ : lowercase letter<br>
◓ : uppercase letter<br>
◑ : positive number<br>
◐ : negative number<br>
⚈ : symbol (first group)<br>
⚉ : symbol (second group)

The other dots can either be set to 0 or 1:<br>
○ : 0<br>
● : 1

The symbols can be written either in matrix or linear format, but not both on the same medium.

Only the symbols matter, any space, newline, etc... only changes the representation format, not the meaning.

## Letters
Letters are divided into 3 groups of 9 letters (8 in the third), each divided into 3 subgroups composed of 3 letters.

`A-I`&nbsp;`1-3`&nbsp;`1`<br>
`J-R`&nbsp;`4-6`&nbsp;`2`<br>
`S-Z`&nbsp;`7-9`&nbsp;`3`

The first column identifies the group, second column the subgroup and third column the particular letter in the subgroup.

Selected group/subgroup/letter is represented by a full dot, unselected by an empty dot.

The full dot used for the group varies depending whether it is lower- or uppercase:<br>
lowercase: ◒<br>
uppercase: ◓

A space is represented by a 3x3 grid of empty dots.<br>
Return is represented using a 3x3 grid of full dots.

### Examples
```
◓ ● ●
○ ○ ○ = A
○ ○ ○

○ ● ○
◓ ○ ○ = L
○ ○ ●

◓ ● ●  ○ ○ ○  ◒ ● ○  ● ● ●  ◓ ● ○
○ ○ ○  ○ ○ ○  ○ ○ ●  ● ● ●  ○ ○ ○ = "A b\nC"
○ ○ ○  ○ ○ ○  ○ ○ ○  ● ● ●  ○ ○ ●

◓○○○○●○●○
◒○○○●○○●○
○◒○●○○○○●
○◒○●○○○○●
○◒○○●○○○●
○○○○○○○○○
○○◓○●○○●○
○◒○○●○○○●
○◒○○○●○○●
○◒○●○○○○●
◒○○○●○●○○ = Hello World
```

## Numbers
Numbers are expressed in binary format. A full dot represents 1, an empty dot represents 0.<br>
The upper-left dot is reserved for the sign, positive or negative. The other dots are counted starting from the middle of the first column, top to bottom then left to right, each representing and increasing power of two.

Powers:<br>
`±`&nbsp;`2`&nbsp;`5`<br>
`0`&nbsp;`3`&nbsp;`6`<br>
`1`&nbsp;`4`&nbsp;`7`

Values:<br>
`±`&nbsp;`04`&nbsp;`32`<br>
`1`&nbsp;`08`&nbsp;`64`<br>
`2`&nbsp;`16`&nbsp;`128`

Sign symbol can be either of the two:<br>
positive: ◑<br>
negative: ◐

Each symbol goes from 0 to ±255, consecutive symbols add bits of increasing power to the number. The dots are counted in both symbols again from top to bottom, left to right starting from the 2nd dot of the first column. In general each number can go from 0 to ±2^(8\*number of symbols)-1. The sign is taken from the first dot, others are ignored but need to be the same.

### Examples
```
◐ ● ●
● ● ● = -255
● ● ●

◑ ● ○
○ ● ● = +92
○ ● ○

◐ ● ● ◐ ● ●
○ ○ ○ ● ○ ● = -63268
○ ○ ○ ● ● ●

◑○○●○○●○○●●●○●●●● = +63268
```

## Symbols
Symbols are represented in a similar way to letters. The first column identifies the group, the second the subgroup and the third the specific symbol.

`Group 1`&nbsp;`Sub-group 1`&nbsp;`Symbol 1`<br>
`Group 2`&nbsp;`Sub-group 2`&nbsp;`Symbol 2`<br>
`Group 3`&nbsp;`Sub-group 3`&nbsp;`Symbol 3`

There are two sections of 27 symbols each, each represented by a different group dot.

Symbols A: ⚈<br>
`+ - *`&nbsp;`^ % ~`&nbsp;`/ ( )`<br>
`\ [ ]`&nbsp;`| { }`&nbsp;`< > =`<br>
`. , ;`&nbsp;`: ! ?`&nbsp;`" ' _`

Symbols B: ⚉<br>
<code># & \`</code>&nbsp;`@ þ Þ`&nbsp;`à á ä`<br>
`ả å â`&nbsp;`ā ã ǎ`&nbsp;`ȧ ç Ç`<br>
`æ Æ œ`&nbsp;`Œ ø Ø`&nbsp;`ß ð Ð`

(The `a` represent a particular accent or diacritic symbol)

For accents a letter must follow the symbol itself, if the symbol is repeated twice then its diacritic sign will be its meaning.

### Examples
```
◑ ○ ○  ⚈ ○ ●  ◐ ○ ●  ○ ○ ○  ◐ ○ ○
○ ● ○  ○ ○ ○  ○ ○ ○  ⚈ ○ ○  ● ○ ○ = '8 / -2 = -4'
○ ○ ○  ○ ● ○  ○ ○ ○  ○ ● ●  ○ ○ ○

⚉ ○ ○  ◒ ○ ○
○ ○ ●  ○ ● ● = é
○ ● ○  ○ ○ ○

⚉○○○○●○●○◒○○○●○○●○ = é
```
